"use strict"

const fs = require('fs');
const fsPromises = fs.promises;

module.exports = async (fnEvent, fnContext, cb) => {

    // console.log('fnContext:', fnContext);
    // console.log('fnEvent:', JSON.stringify(fnEvent));

    if (fnEvent.method !== 'GET') {
      //callback.status(400).fail('Bad Request');
      cb('Bad Request');
      return;
    }

    let path = fnEvent.path
    console.log('path', path)

    let headers = {
      'Content-Type': '',
    };

    if (/.*\.js/.test(path)) {
      headers['Content-Type'] = 'application/javascript';
    } else if (/.*\.css/.test(path)) {
      headers['Content-Type'] = 'text/css';
    } else if (/.*\.ico/.test(path)) {
      headers['Content-Type'] = 'image/x-icon';
    } else if (/.*\.json/.test(path)) {
      headers['Content-Type'] = 'application/json';
    } else if (/.*\.map/.test(path)) {
      headers['Content-Type'] = 'application/octet-stream';
    }
    if (headers['Content-Type']) {
      fnContext.headers(headers);
    }
  
    let contentPath = `${__dirname}/openfaas-app${path}`;

    if (!headers['Content-Type']) {
      contentPath = `${__dirname}/openfaas-app/index.html`;
    }

    let fileAccess = true;

    await fsPromises.access(contentPath, fs.constants.R_OK).catch(() => fileAccess = false);
  
    if (fileAccess) {
      let data = await fsPromises.readFile(contentPath, 'utf8')
      let content = data.toString();
      cb(null, content); 
    } else {
        fnContext.status(404);
    }
}